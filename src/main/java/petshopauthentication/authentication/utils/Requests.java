package petshopauthentication.authentication.utils;

import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import petshopauthentication.authentication.dtos.EmployeeDto;

public class Requests {

    private static RestTemplate restTemplate = new RestTemplate();
    private static String uriAnimalManipulation = "http://localhost:8080";

    public static EmployeeDto findEmployeeById(Long id){
        String uri = uriAnimalManipulation + "/employee/" + id;

        EmployeeDto employeeDto;
        try {
            employeeDto = restTemplate.getForObject(uri, EmployeeDto.class);
        } catch (HttpClientErrorException hce){
            employeeDto = null;
        }

        return employeeDto;
    }

}

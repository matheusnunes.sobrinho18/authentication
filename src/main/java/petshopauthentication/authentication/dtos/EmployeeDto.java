package petshopauthentication.authentication.dtos;

import lombok.*;
import petshopauthentication.authentication.enums.EmployeeFunction;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class EmployeeDto {

    @NotNull
    private Long id;

    @NotBlank
    private String name;

    @NotNull
    private EmployeeFunction function;

}

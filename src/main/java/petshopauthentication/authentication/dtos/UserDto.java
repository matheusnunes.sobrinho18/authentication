package petshopauthentication.authentication.dtos;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserDto {

    @NotBlank
    private String userName;

    @NotBlank
    private String password;

    @NotNull
    private Long employeeId;

}

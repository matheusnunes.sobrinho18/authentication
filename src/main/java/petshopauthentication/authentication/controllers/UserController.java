package petshopauthentication.authentication.controllers;

import lombok.AllArgsConstructor;
import lombok.Builder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import petshopauthentication.authentication.dtos.UserDto;
import petshopauthentication.authentication.models.UserModel;
import petshopauthentication.authentication.services.UserService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Builder
@AllArgsConstructor
@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/user")
public class UserController {

    private UserService userService;

    @PostMapping
    public ResponseEntity<Object> saveUser(@RequestBody @Valid UserDto userDto){

        if(!userService.existsEmployee(userDto.getEmployeeId())){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Employee not exists");
        }

        if(!userService.isASecretary(userDto.getEmployeeId())){
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body("Employee function is not allowed");
        }

        if(userService.existsUser(userDto)){
            return ResponseEntity.status(HttpStatus.CONFLICT).body("This employee already has a LogIn");
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(userService.save(userDto));
    }

    @GetMapping
    public ResponseEntity<List<UserModel>> findAllUsers(){
        return ResponseEntity.status(HttpStatus.OK).body(userService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> findOneUser(@PathVariable(value = "id") Long id){
        Optional<UserModel> userModelOptional = userService.findById(id);

        if(!userModelOptional.isPresent()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not exist");
        }

        return ResponseEntity.status(HttpStatus.FOUND).body(userModelOptional.get());
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable(value = "id") Long id, @RequestBody @Valid UserDto userDto){

        if(!userService.findById(id).isPresent()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not exist");
        }

        if(!userService.existsEmployee(userDto.getEmployeeId())){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Employee not exists");
        }

        if(!userService.isASecretary(userDto.getEmployeeId())){
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body("Employee function is not allowed");
        }

        return ResponseEntity.status(HttpStatus.OK).body(userService.update(id, userDto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable(value = "id") Long id){
        Optional<UserModel> userModelOptional = userService.findById(id);

        if(!userModelOptional.isPresent()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not exist");
        }

        userService.delete(userModelOptional.get());
        return ResponseEntity.status(HttpStatus.OK).body("User deleted successfully!");
    }

}

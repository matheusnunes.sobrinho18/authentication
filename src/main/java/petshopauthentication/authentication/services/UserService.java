package petshopauthentication.authentication.services;

import lombok.AllArgsConstructor;
import lombok.Builder;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import petshopauthentication.authentication.dtos.EmployeeDto;
import petshopauthentication.authentication.dtos.UserDto;
import petshopauthentication.authentication.models.UserModel;
import petshopauthentication.authentication.repositories.UserRepository;
import petshopauthentication.authentication.utils.Requests;

import java.util.List;
import java.util.Optional;

import static petshopauthentication.authentication.enums.EmployeeFunction.SECRETARY;

@Service
@Builder
@AllArgsConstructor
public class UserService {

    private UserRepository userRepository;

    public boolean existsUser(UserDto userDto){
        return userRepository.existsByEmployeeId(userDto.getEmployeeId());
    }

    public boolean existsEmployee(Long employeeId){
        EmployeeDto employeeDto = Requests.findEmployeeById(employeeId);
        return employeeDto != null;
    }

    public boolean isASecretary(Long employeeId){
        EmployeeDto employeeDto = Requests.findEmployeeById(employeeId);

        if(employeeDto.getFunction() == SECRETARY){
            return true;
        } else {
            return false;
        }
    }

    public UserModel save(UserDto userDto) {
        UserModel userModel = new UserModel();
        BeanUtils.copyProperties(userDto, userModel);
        return userRepository.save(userModel);
    }

    public List<UserModel> findAll() {
        return userRepository.findAll();
    }

    public Optional<UserModel> findById(Long id) {
        return userRepository.findById(id);
    }

    public UserModel update(Long id, UserDto userDto) {
        UserModel userModel = new UserModel();
        BeanUtils.copyProperties(userDto, userModel);
        userModel.setId(id);
        return userRepository.save(userModel);
    }

    public void delete(UserModel userModel) {
        userRepository.delete(userModel);
    }
}
